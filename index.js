const express = require('express');//import express
const path = require('path');//import path của express
const app = express();//tạo ứng dụng express
const port = 8000;//khai báo cổng

//định nghĩa middleware để truy vấn các tệp tĩnh trong thư mục views
app.use(express.static('views'));

//định nghĩa route cho trang chủ
app.get('/', (req, res) => {
    const indexPath = path.join(__dirname, './views/landing.html');//trỏ đường dẫn đến file trang chủ
    res.sendFile(indexPath);//gửi file về trình duyệt
});

//Khởi tạo cổng
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});