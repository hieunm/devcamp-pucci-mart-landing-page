"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Tổng số pets trên hệ thống
var gTotalPets = 0; //lát gán sau

// Tham số limit (Số lượng pet tối đa trên 1 trang)
var gPerpage = 8;

//URL API
var gBASE_URL = "https://pucci-mart.onrender.com/api";

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  //Gọi hàm tải trang
  onPageLoading();
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  "use strict";
  // Thực hiện xử lý hiển thị của trang đầu tiên
  // Các trang tiếp theo gán onclick trong nút phân trang và gọi tương tự trang đầu tiên
  createPage(1);
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// Hàm xử lý tạo trang
function createPage(paramPagenum) {
  "use strict";
  $("#spinner-pagination").removeClass("d-none"); //Hiển thị spinner
  //call API lấy danh sách pets phân trang
  callAPIGetPetsPagination(gPerpage, paramPagenum);
}

// Hàm call API lấy danh sách pets phân trang và xử lý hiển thị dựa vào 2 tham số phân trang
function callAPIGetPetsPagination(paramPerpage, paramPagenum) {
  "use strict";
  const vQueryParams = new URLSearchParams({
    _limit: paramPerpage,
    _page: paramPagenum - 1,
  });

  $.ajax({
    type: "get",
    url: gBASE_URL + "/pets?" + vQueryParams.toString(),
    dataType: "json",
    success: function (paramData) {
      console.log(paramData);
      gTotalPets = paramData.count; //Gán giá trị cho biến toàn cục tổng số pets lấy đc
      displayDataGetPetsPagination(paramData); //Gọi hàm xử lý hiển thị lấy danh sách Pets phân trang
      createPagination(paramPagenum); // Gọi hàm tạo thanh phân trang
      $("#spinner-pagination").addClass("d-none"); //Ẩn spinner
    },
    error: function (error) {
      console.log(error);
    },
  });
}

//Hàm xử lý hiển thị lấy danh sách pets có phân trang
function displayDataGetPetsPagination(paramData) {
  "use strict";
  //Xóa trắng danh sách cũ
  $("#pets-container").html("");
  //Dùng Object.values đổi giá trị order trả về thành 1 mảng để dùng forEach
  let vPetsArr = Object.values(paramData.rows);
  if (vPetsArr.length > 0) {
    vPetsArr.forEach((paramPet) => {
      $("#pets-container").append(`
            <div class="col-12 col-md-6 col-lg-3 mb-4">
                <div class="pet-card card">
                    <div class="pet-card-top mx-auto">
                        <img src="${
                            paramPet.imageUrl
                        }" class="card-img-top img-fluid mx-auto" alt="Pet Image">
                        ${
                            paramPet.discount > 0
                            ? `<span class="card-discount badge rounded-circle text-center d-inline-flex justify-content-center align-items-center">${paramPet.discount}%</span>`
                            : ""
                        }
                    </div>
                    <span class="card-id d-none">${paramPet.id}</span>
                    <span class="card-type d-none">${paramPet.type}</span>
                    <span class="card-createdAt d-none">${
                      paramPet.createdAt
                    }</span>
                    <span class="card-updatedAt d-none">${
                      paramPet.updatedAt
                    }</span>
                    <div class="card-body">
                        <p class="card-name">${paramPet.name}</p>
                        <p class="card-description w-100 text-center" data-toggle="tooltip" data-placement="bottom" title="${
                          paramPet.description
                        }">${paramPet.description}</p>
                        <div class="row">
                            <p class="card-promotionPrice">$${
                              paramPet.promotionPrice
                            }</p> 
                            <p class="card-price">$${paramPet.price}</p> 
                        </div>
                    </div>
                </div>
            </div>
        `);
    });
  }
}

// Hàm tạo thanh phân trang
function createPagination(vPagenum) {
  // Xóa trắng phần tử cũ
  $("#pagination-container").html("");
  // Tổng số trang. Math ceil để lấy số sản phẩm tối đa có được. Ví dụ: 10 / 3 = 3.33 => Cần 4 trang hiển thị
  // Khai báo ở đây để chờ gTotalOrders được gán giá trị xong
  let vTotalPages = Math.ceil(gTotalPets / gPerpage);

  // Nếu tran hiện tại là trang 1 thì nút Prev sẽ bị disable
  if (vPagenum == 1) {
    $("#pagination-container").append(
      "<li class='page-item disabled previous mx-1'><a href='javascript:void(0)' class='page-link rounded-circle'><i class='fas fa-chevron-circle-left'></i></a></li>"
    );
  } else {
    $("#pagination-container").append(
      "<li class='page-item mx-1' onclick='createPage(" +
        (vPagenum - 1) +
        ")'><a href='javascript:void(0)' class='page-link rounded-circle'><i class='fas fa-chevron-circle-left text-success'</a></li>"
    );
  }

  // // Hiển thị 3 trang trong phạm vi
  // for (var bi = 0; bi <= 2; bi++) {
  //     if (vPagenum == (vPagenum + bi)) {//trường hợp bi = 0, viết code như này hơi thừa
  //         $("#pagination-container").append("<li class='page-item disabled'><a href='javascript:void(0)' class='page-link'>" + (vPagenum + bi) + "</a></li>");
  //     } else {
  //         if ((vPagenum + bi) <= vTotalPages) {
  //             $("#pagination-container").append("<li class='page-item' onclick='createPage(" + (vPagenum + bi) + ")'><a href='javascript:void(0)' class='page-link'>" + (vPagenum + bi) + "</a></li>");
  //         }
  //     }
  // }

  // Nếu tran hiện tại là trang cuối cùng thì nút Next sẽ bị disable
  if (vPagenum == vTotalPages) {
    $("#pagination-container").append(
      "<li class='page-item disabled mx-1'><a href='javascript:void(0)' class='page-link rounded-circle'><i class='fas fa-chevron-circle-right'></a></li>"
    );
  } else {
    $("#pagination-container").append(
      "<li class='page-item next mx-1' onclick='createPage(" +
        (vPagenum + 1) +
        ")'><a href='javascript:void(0)' class='page-link rounded-circle'><i class='fas fa-chevron-circle-right text-success'></i></a></li>"
    );
  }
}
